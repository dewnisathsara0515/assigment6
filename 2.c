#include <stdio.h>
void a(int x);
void b(int y);
int NR=1;
int z;
void a(int x)
{
    if(x>0)
    {
        b(NR);
        printf("\n");
        NR++;
        a(x-1);
    }
}
void b(int y)
{
    if(y>0)
    {
        printf("%d",y);
        b(y-1);
    }
}
int main()
{
    printf("Enter the No. of Rows : ");
    scanf("%d",&z);
    a(z);
    return 0;
}
